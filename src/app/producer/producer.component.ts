import {Component, OnInit} from '@angular/core';
import {ProducerService} from './producer.service';

@Component({
  selector: 'app-producer',
  templateUrl: './producer.component.html',
  styleUrls: ['./producer.component.css']
})
export class ProducerComponent implements OnInit {

  producerServiceLoaded = true;
  message = '';
  badStatusCode = false;

  constructor(protected producerService: ProducerService) {
  }

  ngOnInit() {
  }

  onSendClicked(count: number) {
    this.producerServiceLoaded = false;
    this.badStatusCode = false;
    this.message = 'Loading...';
    this.producerService.sendRandomMessages(count)
      .subscribe(
        data => {
          this.message = JSON.stringify(data);
          this.producerServiceLoaded = true;
        },
        error2 => {
          console.log(error2);
          this.badStatusCode = true;
          this.message = 'Unable to send message';
          this.producerServiceLoaded = true;
        },
        () => {}
      );
  }

}
