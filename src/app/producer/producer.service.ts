import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {ApiLinks} from '../shared/ApiLinks';

@Injectable()
export class ProducerService {

  constructor(protected httpClient: HttpClient) {
  }

  sendRandomMessages(count: number) {
    const url = ApiLinks.producerRandomMessageAPI + count.toString();
    return this.httpClient.post(url, null);
  }
}
