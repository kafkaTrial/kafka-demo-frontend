export class ApiLinks {
  public static producerRandomMessageAPI = 'http://localhost:8080/producer/m/r/';
  public static consumerUrl = [
    'http://localhost:8083/consumer/m/all',
    'http://localhost:8084/consumer/m/all',
    'http://localhost:8085/consumer/m/all'
  ];
}
