import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';

@Injectable()
export class ConsumersService {

  constructor(private httpClient: HttpClient) {
  }

  getAllRecords(url: string) {
    return this.httpClient.get(url);
  }

  clearAllRecords(url: string) {
    return this.httpClient.delete(url);
  }

}
