export interface ConsumerRecord {
  partition: number;
  offset: number;
  key: string;
  superPnr: string;
  paymentId: string;
  status: string;
}
