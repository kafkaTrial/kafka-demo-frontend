import {Component, Input, OnInit} from '@angular/core';
import {ConsumerRecord} from '../consumers.model';

@Component({
  selector: 'app-consumer-data-view',
  templateUrl: './consumer-data-view.component.html',
  styleUrls: ['./consumer-data-view.component.css']
})
export class ConsumerDataViewComponent implements OnInit {

  value = '';
  @Input('consumerRecord') consumerRecord: ConsumerRecord;

  constructor() {
  }

  ngOnInit() {
    this.value = JSON.stringify({
      'superPnr': this.consumerRecord.superPnr,
      'paymentId': this.consumerRecord.paymentId,
      'status': this.consumerRecord.status
    });
  }

}
