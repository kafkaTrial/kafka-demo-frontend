import {Component, Input, OnInit} from '@angular/core';
import {ConsumersService} from '../consumers.service';
import {ApiLinks} from '../../shared/ApiLinks';
import {ConsumerRecord} from '../consumers.model';

@Component({
  selector: 'app-consumer',
  templateUrl: './consumer.component.html',
  styleUrls: ['./consumer.component.css']
})
export class ConsumerComponent implements OnInit {

  @Input() cid: number;
  @Input() gid: string;
  loadedList = true;
  badStatusCode = false;
  message = 'Please Click Refresh to fetch messages';
  displayMessage = true;
  consumerRecordList: ConsumerRecord[];
  allMessagesUrl: string;

  constructor(protected consumerService: ConsumersService) {
  }

  ngOnInit() {
    this.allMessagesUrl = ApiLinks.consumerUrl[this.cid - 1];
  }

  onRefreshClicked() {
    this.message = 'Loading..';
    this.displayMessage = true;
    this.badStatusCode = false;
    this.loadedList = false;
    this.consumerService.getAllRecords(this.allMessagesUrl)
      .subscribe(
        data => {
          this.consumerRecordList = <ConsumerRecord[]> data;
          this.loadedList = true;
          this.displayMessage = false;
        },
        error2 => {
          this.badStatusCode = true;
          this.displayMessage = true;
          this.message = 'Unable to fetch messages / The service might be down or temporarily unavailable';
        }
      );
  }

  onClearClicked() {
    this.message = 'Loading';
    this.displayMessage = true;
    this.badStatusCode = false;
    this.loadedList = false;
    this.consumerService.clearAllRecords(this.allMessagesUrl)
      .subscribe(
        data => {
          this.onRefreshClicked();
        },
        error2 => {
          this.onRefreshClicked();
        }
      );
  }

}
