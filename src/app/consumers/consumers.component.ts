import {Component, OnInit} from '@angular/core';

@Component({
  selector: 'app-consumers',
  templateUrl: './consumers.component.html',
  styleUrls: ['./consumers.component.css']
})
export class ConsumersComponent implements OnInit {

  consumers = [{cid: 1, gid: 'cg1'}, {cid: 2, gid: 'cg1'}, {cid: 3, gid: 'cg2'}];

  constructor() {
  }

  ngOnInit() {
  }

}
