import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';


import { AppComponent } from './app.component';
import { ProducerComponent } from './producer/producer.component';
import { ConsumersComponent } from './consumers/consumers.component';
import { ConsumerComponent } from './consumers/consumer/consumer.component';
import { ConsumerDataViewComponent } from './consumers/consumer-data-view/consumer-data-view.component';
import {ConsumersService} from './consumers/consumers.service';
import {ProducerService} from './producer/producer.service';
import {HttpClientModule} from '@angular/common/http';
import {ReactiveFormsModule} from '@angular/forms';


@NgModule({
  declarations: [
    AppComponent,
    ProducerComponent,
    ConsumersComponent,
    ConsumerComponent,
    ConsumerDataViewComponent
  ],
  imports: [
    BrowserModule,
    ReactiveFormsModule,
    HttpClientModule
  ],
  providers: [ConsumersService, ProducerService],
  bootstrap: [AppComponent]
})
export class AppModule { }
